﻿=== Ultimate Member - Debug tools ===
Author URI: https://ultimatemember.com/
Plugin URI: https://ultimatemember.com/
Contributors: ultimatemember, nsinelnikov
Tags: ultimatemember, debug, log, tools
Requires at least: 5.0
Tested up to: 5.3
Stable tag: 1.4.3
License: GNU Version 2 or Any Later Version
License URI: http://www.gnu.org/licenses/gpl-3.0.txt

== Description ==

Simple WordPress plugin for logging and testing

= Key Features: =

* Display content of the debug.log file, color errors and warnings.
* Filter content of the debug.log file by key word.
* Log defined hooks to um_hook.log file, add information about request and debug backtrace (optional).
* Log mails to um_mail.log file, add information about request and debug backtrace (optional).
* Log mails after defined hooks
* Log mails with defined subjects
* Clear debug.log file
* Clear um_hook.log file
* Clear um_mail.log file

== Changelog ==

= 1.4.3: April 5, 2020 =

* Fixed: Hook log settings

= 1.4.2: March 28, 2020 =

* Fixed: Frontend styles

= 1.4.1: October 31, 2019 =

* Added: Hook log

= 1.4.0: October 30, 2019 =

* Added: defined hooks
* Added: options "Log mails after these hooks" and "Log mails with these subjects"