<?php
/**
 * Plugin Name: Ultimate Member - Debug tools
 * Description: Debug log, Hook log, Mail log, Eval custom code. See menu item "Tools"
 * Version: 1.4.3
 * Author: Ultimate Member
 * Author URI: http://ultimatemember.com/
 * Text Domain: um-debug
 */

/**
 * The main class of the UM extension "Debug tools"
 */
class umd {

	const LOGFILEPATH = '/wp-content/debug.log';
	const LOCALHOST = '127.0.0.1';

	private $timestart;
	private $timelast;
	private $dump = array();
	private $prof = array();
	private $vars = array();
	private $log_hook = false;
	private $log_hook_backtrace = false;
	private $log_hook_hooks = array();
	private $log_hook_rows = 99;
	private $log_mails = true;
	private $log_mails_backtrace = false;
	private $log_mails_hooks = array();
	private $log_mails_subjects = array();
	private $log_mails_rows = 99;
	private $log_debug_rows = 99;

	public function __construct() {

		// Files
		$upload_dir = wp_upload_dir();
		$this->logfilepath = ABSPATH . self::LOGFILEPATH;
		$this->loghookpath = $upload_dir['basedir'] . DIRECTORY_SEPARATOR . 'um_hook.log';
		$this->logmailpath = $upload_dir['basedir'] . DIRECTORY_SEPARATOR . 'um_mail.log';

		// Time
		$this->timestart = $this->timelast = microtime( true );

		// Settings
		$this->log_debug_ip = (array) get_option( 'umd_log_debug_ip', self::LOCALHOST );
		$this->log_debug_rows = (int) get_option( 'umd_log_debug_rows', $this->log_debug_rows );
		//
		$this->log_hook = (int) get_option( 'umd_log_hook', $this->log_hook );
		$this->log_hook_backtrace = (int) get_option( 'umd_log_hook_backtrace', $this->log_hook_backtrace );
		$this->log_hook_hooks = (array) get_option( 'umd_log_hook_hooks', $this->log_hook_hooks );
		$this->log_hook_rows = (int) get_option( 'umd_log_hook_rows', $this->log_hook_rows );
		//
		$this->log_mails = (int) get_option( 'umd_log_mails', $this->log_mails );
		$this->log_mails_backtrace = (int) get_option( 'umd_log_mails_backtrace', $this->log_mails_backtrace );
		$this->log_mails_hooks = (array) get_option( 'umd_log_mails_hooks', $this->log_mails_hooks );
		$this->log_mails_subjects = (array) get_option( 'umd_log_mails_subjects', $this->log_mails_subjects );
		$this->log_mails_rows = (int) get_option( 'umd_log_mails_rows', $this->log_mails_rows );

		// Page for testing
		add_action( 'admin_menu', array( $this, 'add_submenu' ), 20 );
		add_action( 'admin_enqueue_scripts', array( $this, 'add_style' ), 20 );

		// Profiling
		add_action( 'umd_profiling', array( $this, 'save_microtime' ) );

		// Show debug_backtrace in the footer
		if ( defined( 'WP_DEBUG' ) && WP_DEBUG ) {
			add_action( 'admin_footer', array( $this, 'show' ), 99 );
			add_action( 'wp_footer', array( $this, 'show' ), 99 );
		}

		// Execute handlers
		add_action( 'admin_init', array( $this, 'execute_handlers' ), 20 );

		// Log these hooks
		if ( $this->log_hook ) {
			foreach ( (array) $this->log_hook_hooks as $hook ) {
				add_action( $hook, array( $this, 'log_hook' ), 5, 5 );
				add_filter( $hook, array( $this, 'log_hook' ), 5, 5 );
			}
		}

		// Log mails after these hooks
		foreach ( (array) $this->log_mails_hooks as $hook ) {
			add_filter( $hook, function( $data ) {
				$this->log_mails = true;
				return $data;
			}, 10 );
		}

		// Log mails
		add_filter( 'wp_mail', array( $this, 'log_mail' ), 35 );

		// Create files
		if ( !file_exists( $this->loghookpath ) ) {
			file_put_contents( $this->loghookpath, '' );
		}
		if ( !file_exists( $this->logmailpath ) ) {
			file_put_contents( $this->logmailpath, '' );
		}
	}

	public function add_submenu() {
		add_management_page( __( 'UM Debug Log', 'um-debug' ), __( 'UM Debug Log', 'um-debug' ), 'administrator', 'um_debug_log', array( $this, 'render_debug_log_page' ) );
		add_management_page( __( 'UM Hook Log', 'um-debug' ), __( 'UM Hook Log', 'um-debug' ), 'administrator', 'um_hook_log', array( $this, 'render_hook_log_page' ) );
		add_management_page( __( 'UM Mail Log', 'um-debug' ), __( 'UM Mail Log', 'um-debug' ), 'administrator', 'um_mail_log', array( $this, 'render_mail_log_page' ) );
		add_management_page( __( 'UM Testing Page', 'um-debug' ), __( 'UM Testing Page', 'um-debug' ), 'administrator', 'um_testing', array( $this, 'render_testing_page' ) );
	}

	public function add_style() {
		wp_enqueue_style( 'um-style', plugins_url( 'um-style.css', __FILE__ ) );
	}

	public function clear_debug_log() {
		if ( is_file( $this->logfilepath ) ) {
			file_put_contents( $this->logfilepath, '' );

			if ( wp_redirect( admin_url( 'tools.php?page=um_debug_log' ) ) ) {
				exit;
			}
		}
	}

	public function clear_hook_log() {
		if ( is_file( $this->loghookpath ) ) {
			file_put_contents( $this->loghookpath, '' );

			if ( wp_redirect( admin_url( 'tools.php?page=um_hook_log' ) ) ) {
				exit;
			}
		}
	}

	public function clear_mail_log() {
		if ( is_file( $this->logmailpath ) ) {
			file_put_contents( $this->logmailpath, '' );

			if ( wp_redirect( admin_url( 'tools.php?page=um_mail_log' ) ) ) {
				exit;
			}
		}
	}

	public function execute_handlers() {

		$handlers = array(
			'clear_debug_log',
			'clear_hook_log',
			'clear_mail_log',
			'update_options'
		);

		if ( !empty( $_REQUEST['action'] ) && in_array( $_REQUEST['action'], $handlers ) && method_exists( $this, $_REQUEST['action'] ) ) {
			call_user_func( array( $this, $_REQUEST['action'] ) );
		}
	}

	public function get_log_mail_to() {

		$to_arr = array();

		$mail_log_arr = file( $this->logmailpath );

		foreach ( $mail_log_arr as $row ) {
			if ( preg_match( '/^To: (\S+@\S+)/i', $row, $match ) ) {
				array_push( $to_arr, $match[1] );
			}
		}

		$unique_to_arr = array_unique( $to_arr );
		sort( $unique_to_arr );

		return $unique_to_arr;
	}

	public function log_hook( $arg1 = null, $arg2 = null, $arg3 = null, $arg4 = null, $arg5 = null ) {

		$log = "\r\n"
			. "[" . date( 'Y-m-d H:i:s' ) . "]\r\n"
			. "Hook: " . current_filter() . "\r\n";

		$args = func_get_args();
		if ( $args ) {
			$argsjson = json_encode( $args );
			$log .= "Args: $argsjson\r\n";
		}

		// Request data
		$log .= "---\r\n"
			. "Request:\r\n"
			. "REMOTE_ADDR: {$_SERVER['REMOTE_ADDR']}\r\n"
			. "REQUEST_URI: {$_SERVER['REQUEST_URI']}\r\n";

		// Debug Backtrace
		if ( $this->log_hook_backtrace ) {
			$log .= "---\r\n"
				. "Debug Backtrace:\r\n";
			foreach ( debug_backtrace() as $value ) {
				$text_file = isset( $value['file'] ) ? $value['file'] : '';
				$text_line = isset( $value['line'] ) ? $value['line'] : '';
				$log .= "  $text_file line $text_line\r\n";
			}
		}

		file_put_contents( $this->loghookpath, $log, FILE_APPEND );
		unset( $log );

		return $arg1;
	}

	public function log_mail( $mail_info ) {

		// Log mails with these subjects
		if ( !$this->log_mails && $this->log_mails_subjects ) {
			foreach ( $this->log_mails_subjects as $subject ) {
				if ( substr_count( $mail_info['subject'], $subject ) ) {
					$this->log_mails = true;
				}
			}
		}

		if ( $this->log_mails ) {

			$mailto = is_array( $mail_info['to'] ) ? implode( ', ', $mail_info['to'] ) : $mail_info['to'];

			$subject = $mail_info['subject'];

			$message = preg_replace( '/\s+/i', ' ', strip_tags( $mail_info['message'] ) );

			$headers = is_array( $mail_info['headers'] ) ? implode( "\r\n", $mail_info['headers'] ) : trim( $mail_info['headers'] );


			$log = "\r\n"
				. "[" . date( 'Y-m-d H:i:s' ) . "]\r\n"
				. "To: $mailto\r\n"
				. "Subject: $subject\r\n"
				. "Message:\r\n"
				. "$message\r\n";

			// Headers
			if ( !empty( $headers ) ) {
				$log .= "---\r\n"
					. "Headers:\r\n"
					. "$headers\r\n";
			}

			// Request data
			$log .= "---\r\n"
				. "Request:\r\n"
				. "REMOTE_ADDR: {$_SERVER['REMOTE_ADDR']}\r\n"
				. "REQUEST_URI: {$_SERVER['REQUEST_URI']}\r\n";

			// Debug Backtrace
			if ( $this->log_mails_backtrace ) {
				$log .= "---\r\n"
					. "Debug Backtrace:\r\n";
				foreach ( debug_backtrace() as $value ) {
					$text_file = isset( $value['file'] ) ? $value['file'] : '';
					$text_line = isset( $value['line'] ) ? $value['line'] : '';
					$log .= "  $text_file line $text_line\r\n";
				}
			}

			file_put_contents( $this->logmailpath, $log, FILE_APPEND );
			unset( $log );
		}

		return $mail_info;
	}

	public function render_debug_log_page() {
		$filter_text = filter_input( 0, 'umd_log_debug_filter_text' );
		if ( !$filter_text ) {
			$filter_text = get_option( 'umd_log_debug_filter_text' );
		}

		if ( !file_exists( $this->logfilepath ) ) {
			?>
			<div class="notice notice-error is-dismissible">
				<p><?php _e( 'No file "debug.log".', 'um-debug' ); ?></p>
			</div>
			<?php
			$content = '';
		} else {
			$debug_log_arr = file( $this->logfilepath );
			foreach ( $debug_log_arr as $key => $value ) {
				if ( $filter_text && !substr_count( $value, $filter_text ) ) {
					unset( $debug_log_arr[$key] );
				}
				if ( substr_count( $value, 'PHP Error' ) ) {
					$debug_log_arr[$key] = '<span style="color:darkred;">' . $debug_log_arr[$key] . '</span>';
				}
				if ( substr_count( $value, 'PHP Fatal error' ) ) {
					$debug_log_arr[$key] = '<span style="color:darkred;">' . $debug_log_arr[$key] . '</span>';
				}
				if ( substr_count( $value, 'PHP Notice' ) ) {
					$debug_log_arr[$key] = '<span style="color:darkblue;">' . $debug_log_arr[$key] . '</span>';
				}
				if ( substr_count( $value, 'PHP Warning' ) ) {
					$debug_log_arr[$key] = '<span style="color:darkgoldenrod;">' . $debug_log_arr[$key] . '</span>';
				}
			}

			$content = implode( '</br>', array_slice( array_reverse( $debug_log_arr ), 0, $this->log_debug_rows ) );
		}
		?>
		<div class="wrap">
			<h1 class="wp-heading-inline"><?php _e( 'UM Debug Log', 'um-debug' ); ?></h1>

			<form method="POST" class="um-debug">
				<input type="hidden" name="page" value="um_debug_log">
				<table class="widefat striped">
					<thead>
						<tr>
						<th scope="row">
						<label><?php _e( 'Actions' ); ?></label>
						</th>
						<td>
						<button type="submit" name="action" value="clear_debug_log" class="button button-primary"><?php _e( 'Clear log' ); ?></button>
						<button type="submit" name="action" value="update_options" class="button button-primary"><?php _e( 'Save settings' ); ?></button>
						</td>
						</tr>
					</thead>
					<tbody>
						<tr>
						<th scope="row">
						<label><?php _e( 'Settings' ); ?></label>
						</th>
						<td>
						<label><input type="number"  name="umd_log_debug_rows" value="<?php echo $this->log_debug_rows; ?>" title="<?php _e( 'Show rows' ); ?>" class="small-text" /></label>
						<label><input type="text"  name="umd_log_debug_ip" value="<?php echo implode( ',', $this->log_debug_ip ); ?>" title="<?php _e( 'IP for testing' ); ?>" class="regular-input" /></label>
						<label><input type="text"  name="umd_log_debug_filter_text" value="<?php echo $filter_text; ?>" placeholder="<?php _e( 'Filter text' ); ?>" title="<?php _e( 'Filter by text' ); ?>" class="regular-input" /></label>
						<button type="submit" name="action" value="filter_debug_log" class="button"><?php _e( 'Filter' ); ?></button>
						</td>
						</tr>
					</tbody>
				</table>
			</form>
			<br />

			<?php echo $content; ?>
		</div>
		<?php
	}

	public function render_hook_log_page() {
		?>
		<div class="wrap">
			<h1 class="wp-heading-inline"><?php _e( 'UM Hook Log', 'um-debug' ); ?></h1>
			<form method="POST" class="um-debug">
				<input type="hidden" name="page" value="um_hook_log">
				<table class="widefat striped">
					<thead>
						<tr>
						<th scope="row">
						<label><?php _e( 'Actions' ); ?></label>
						</th>
						<td>
						<button type="submit" name="action" value="clear_hook_log" class="button button-primary"><?php _e( 'Clear log' ); ?></button>
						<button type="submit" name="action" value="update_options" class="button button-primary"><?php _e( 'Save settings' ); ?></button>
						</td>
						</tr>
					</thead>
					<tbody>
						<tr>
						<th scope="row">
						<label><?php _e( 'Settings', 'um-debug' ); ?></label>
						</th>
						<td>
						<label><input type="number" name="umd_log_hook_rows" value="<?php echo esc_attr( $this->log_hook_rows ); ?>" class="small-text" title="<?php esc_attr_e( 'Show rows', 'um-debug' ); ?>" /></label>
						<span class="button">
							<strong><?php _e( 'Enable', 'um-debug' ); ?></strong>
							<label><input type="radio" name="umd_log_hook" value="0" <?php checked( 0, $this->log_hook ) ?>> <?php _e( 'OFF', 'um-debug' ); ?></label>
							<label><input type="radio" name="umd_log_hook" value="1" <?php checked( 1, $this->log_hook ) ?>> <?php _e( 'ON', 'um-debug' ); ?></label>
						</span>
						<span class="button">
							<strong><?php _e( 'Log backtrace', 'um-debug' ); ?></strong>
							<label><input type="radio" name="umd_log_hook_backtrace" value="0" <?php checked( 0, $this->log_hook_backtrace ) ?>> <?php _e( 'NO', 'um-debug' ); ?></label>
							<label><input type="radio" name="umd_log_hook_backtrace" value="1" <?php checked( 1, $this->log_hook_backtrace ) ?>> <?php _e( 'YES', 'um-debug' ); ?></label>
						</span>
						</td>
						</tr>
						<tr>
						<th scope="row">
						<label><?php _e( 'Hooks' ); ?></label>
						</th>
						<td>
							<textarea name="umd_log_hook_hooks" class="code large-text" cols="35" rows="3" placeholder="<?php esc_attr_e( 'Log these hooks', 'um-debug' ); ?>" title="<?php esc_attr_e( 'Log these hooks', 'um-debug' ); ?>"><?php echo implode( ',', $this->log_hook_hooks ); ?></textarea>
						</td>
						</tr>
					</tbody>
				</table>
			</form>
			<br />

			<?php
			$log_arr = file( $this->loghookpath );
			$lines = count( $log_arr );
			$start = max( 0, $lines - $this->log_hook_rows );
			for ( $i = $start; $i < $lines; $i++ ) {
				echo htmlspecialchars( $log_arr[$i] ) . '</br>';
			}
			?>
		</div>
		<?php
	}

	public function render_mail_log_page() {
		?>
		<div class="wrap">
			<h1 class="wp-heading-inline"><?php _e( 'UM Mail Log', 'um-debug' ); ?></h1>
			<form method="POST" class="um-debug">
				<input type="hidden" name="page" value="um_mail_log">
				<table class="widefat striped">
					<thead>
						<tr>
						<th scope="row">
						<label><?php _e( 'Actions', 'um-debug' ); ?></label>
						</th>
						<td>
						<button type="submit" name="action" value="clear_mail_log" class="button button-primary"><?php _e( 'Clear log', 'um-debug' ); ?></button>
						<button type="submit" name="action" value="update_options" class="button button-primary"><?php _e( 'Save settings', 'um-debug' ); ?></button>
						<button type="submit" name="umd_log_mails_show" value="list_to" class="button"><?php _e( 'List "to"', 'um-debug' ); ?></button>
						</td>
						</tr>
					</thead>
					<tbody>
						<tr>
						<th scope="row">
						<label><?php _e( 'Settings', 'um-debug' ); ?></label>
						</th>
						<td>
						<label><input type="number" name="umd_log_mails_rows" value="<?php echo esc_attr( $this->log_mails_rows ); ?>" class="small-text" title="<?php esc_attr_e( 'Show rows', 'um-debug' ); ?>" /></label>
						<span class="button">
							<strong><?php _e( 'Enable', 'um-debug' ); ?></strong>
							<label><input type="radio" name="umd_log_mails" value="0" <?php checked( 0, $this->log_mails ) ?>> <?php _e( 'OFF', 'um-debug' ); ?></label>
							<label><input type="radio" name="umd_log_mails" value="1" <?php checked( 1, $this->log_mails ) ?>> <?php _e( 'ON', 'um-debug' ); ?></label>
						</span>
						<span class="button">
							<strong><?php _e( 'Log backtrace', 'um-debug' ); ?></strong>
							<label><input type="radio" name="umd_log_mails_backtrace" value="0" <?php checked( 0, $this->log_mails_backtrace ) ?>> <?php _e( 'NO', 'um-debug' ); ?></label>
							<label><input type="radio" name="umd_log_mails_backtrace" value="1" <?php checked( 1, $this->log_mails_backtrace ) ?>> <?php _e( 'YES', 'um-debug' ); ?></label>
						</span>
						</td>
						</tr>
						<tr>
						<th scope="row">
						<label><?php _e( 'Hooks', 'um-debug' ); ?></label>
						</th>
						<td>
							<textarea name="umd_log_mails_hooks" class="code medium-text" cols="35" rows="3" placeholder="<?php esc_attr_e( 'Log mails after these hooks', 'um-debug' ); ?>" title="<?php esc_attr_e( 'Log mails after these hooks', 'um-debug' ); ?>"><?php echo implode( ',', $this->log_mails_hooks ); ?></textarea>
							<textarea name="umd_log_mails_subjects" class="code medium-text" cols="35" rows="3" placeholder="<?php esc_attr_e( 'Log mails with these subjects', 'um-debug' ); ?>" title="<?php esc_attr_e( 'Log mails with these subjects', 'um-debug' ); ?>"><?php echo implode( ',', $this->log_mails_subjects ); ?></textarea>
						</td>
						</tr>
					</tbody>
				</table>
			</form>
			<br />

			<?php
			$show = filter_input( 0, 'umd_log_mails_show' );

			switch ( $show ) {
				case "list_to":
					echo implode( '<br>', $this->get_log_mail_to() );
					break;

				default:
					$log_arr = file( $this->logmailpath );
					$lines = count( $log_arr );
					$start = max( 0, $lines - $this->log_mails_rows );
					for ( $i = $start; $i < $lines; $i++ ) {
						echo htmlspecialchars( $log_arr[$i] ) . '</br>';
					}
					break;
			}
			?>
		</div>
		<?php
	}

	public function render_testing_page() {
		$code = filter_input( 0, 'code' );
		if ( empty( $code ) && isset( $_SESSION['umd_code'] ) ) {
			$code = $_SESSION['umd_code'];
		} else {
			$_SESSION['umd_code'] = $code;
		}
		?>

		<div class="wrap">
			<h1 class="wp-heading-inline"><?php _e( 'UM Testing Page', 'um-debug' ); ?></h1>
			<div class="">
				<form method="POST" class="um-debug">
					<textarea name="code" rows="10" style="width: 100%;"><?php echo $code; ?></textarea>
					<p><button type="submit" class="button button-primary"><?php _e( 'Eval', 'um-debug' ); ?></button></p>
				</form>
				<hr />
				<?php
				if ( $code ) {
					eval( $code );

					remove_action( 'admin_footer', array( $this, 'show' ), 99 );
					$this->show();
				}
				?>
			</div>
		</div>

		<?php
	}

	public function show() {
		if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
			return;
		}
		if ( !in_array( $_SERVER['REMOTE_ADDR'], $this->log_debug_ip ) ) {
			return;
		}

		if ( !wp_style_is( 'um-style' ) ) {
			echo '<link href="' . plugins_url( 'um-style.css', __FILE__ ) . '" rel="stylesheet" type="text/css"/>';
		}

		if ( $this->dump || $this->prof || $this->vars ) {
			echo '<section class="umd-dump">';

			if ( $this->dump ) {
				echo '<p>' . __( 'UM Backtrace' ) . '</p>';
				foreach ( $this->dump as $key => $value ) {
					echo '<div class="umd-item">', "<p>Backtrace: $key</p>", '<div>';
					foreach ( $value as $k => $v ) {
						echo isset( $v['file'] ) ? "{$v['file']} : {$v['line']}<br />" : '';
					}
					echo '</div>', '</div>';
				}
			}

			if ( $this->prof ) {
				echo '<p>' . __( 'UM Profiling' ) . '</p>';
				foreach ( $this->prof as $key => $value ) {
					echo '<div class="umd-item">', "<p>$value</p>", '</div>';
				}
			}

			if ( $this->vars ) {
				echo '<p>' . __( 'UM Debug Vars' ) . '</p>';
				foreach ( $this->vars as $key => $value ) {
					echo '<div class="umd-item">', "<p>Var: $key</p>", '<div>', '<pre>';
					print_r( $value );
					echo '</pre>', '</div>', '</div>';
				}
			}

			echo '</section>';
		}
	}

	public function save_backtrace( $backtrace, $key = null ) {
		if ( empty( $key ) ) {
			$this->dump[] = $backtrace;
		} elseif ( isset( $this->dump[$key] ) ) {
			$this->dump[$key . count( $this->dump )] = $backtrace;
		} else {
			$this->dump[$key] = $backtrace;
		}
	}

	public function save_microtime( $key = null ) {

		$timecurrent = microtime( true );
		$diff_from_start = number_format( $timecurrent - $this->timestart, 4 );
		$diff_from_prev = number_format( $timecurrent - $this->timelast, 4 );
		$text = "<code>$diff_from_start $diff_from_prev</code> - $key";
		$this->timelast = $timecurrent;

		if ( empty( $key ) ) {
			$this->prof[] = $text;
		} elseif ( isset( $this->prof[$key] ) ) {
			$this->prof[$key . count( $this->prof )] = $text;
		} else {
			$this->prof[$key] = $text;
		}
	}

	public function save_var( $var, $key = null, $dublicate = false ) {
		if ( empty( $key ) ) {
			$this->vars[] = $var;
		} elseif ( isset( $this->vars[$key] ) && $dublicate ) {
			$this->vars[$key . count( $this->vars )] = $var;
		} else {
			$this->vars[$key] = $var;
		}
	}

	public function update_options() {
		if ( empty( $_POST ) ) {
			return;
		}
		foreach ( $_POST as $key => $value ) {
			if ( !preg_match( '/^umd_/i', $key ) ) {
				continue;
			}
			if ( is_string( $value ) && substr_count( $value, ',' ) ) {
				$value = array_map( 'trim', explode( ',', $value ) );
			}
			update_option( $key, $value );
		}
		wp_redirect( $_SERVER['REQUEST_URI'] );
	}

}

function umd( $var = null, $key = null, $dublicate = false ) {
	static $umd = null;

	if ( empty( $umd ) ) {
		$umd = new umd();
		$GLOBALS['umd'] = &$umd;
	}

	if ( !is_null( $var ) ) {
		$umd->save_var( $var, $key, $dublicate );
	}

	return $umd;
}

function umdb( $key = null ) {
	$backtrace = debug_backtrace( 2 );
	array_shift( $backtrace );

	umd()->save_backtrace( $backtrace, $key );

	return $backtrace;
}

umd();
